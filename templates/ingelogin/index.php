<?php

/* ************************************************************************************************************************

Ingelogin

File:			index.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$class = $pageclass->get( 'pageclass_sfx' );

// Template path

$path = 'templates/' . $this->template . '/';

// Remove scripts

unset( $doc->_scripts[ JURI::root(true) . '/media/system/js/mootools-core.js' ] );
unset( $doc->_scripts[ JURI::root(true) . '/media/system/js/core.js' ] );
unset( $doc->_scripts[ JURI::root(true) . '/media/system/js/mootools-more.js' ] );

// Modules

$show_application = $this->countModules( 'application' );
$show_banner = $this->countModules( 'banner' );
$show_bottom = $this->countModules( 'bottom' );
$show_home = $this->countModules( 'home' );
$show_menu = $this->countModules( 'menu' );
$show_news = $this->countModules( 'news' );
$show_social_media = $this->countModules( 'social_media' );
$show_top = $this->countModules( 'top' );

// Params

?>
<!DOCTYPE html>
<html class="no-js" lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="suQA5rq0ra0l-AU-MHbi7WvF_tFuObkGv7O7htQLqi0">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>build/bower_components/foundation-sites/dist/css/foundation-flex.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/animate.css/animate.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/fancybox/dist/jquery.fancybox.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/app.css" rel="stylesheet" type="text/css">
		<!-- Begin Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111120515-1"></script>
		<script>

			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-111120515-1');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php if ( $show_top ) : ?>
		<!-- Begin Top -->
			<section class="top wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<jdoc:include type="modules" name="top" style="xhtml" />
				</div>
			</section>
		<!-- End Top -->
		<?php endif; ?>
		<?php if ( $show_menu ) : ?>
		<!-- Begin Menu -->
			<section class="menu_wrap wow fadeIn show-for-small-only" data-wow-delay="0.5s">
				<div class="top-bar">
					<div class="top-bar-title">
						<span data-responsive-toggle="responsive-menu" data-hide-for="medium">
							<button class="menu-icon dark" type="button" data-toggle></button>
						</span>
						<strong>Menú</strong>
					</div>
					<div id="responsive-menu">
						<div class="top-bar-left">
							<jdoc:include type="modules" name="menu" style="xhtml" />
						</div>
					</div>
				</div>
			</section>
		<!-- End Menu -->
		<?php endif; ?>
		<?php if ( $show_banner ) : ?>
		<!-- Begin Banner -->
			<section class="banner wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="banner" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Banner -->
		<?php endif; ?>
		<!-- Begin Component -->
			<section class="component<?php echo $class; ?> wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="component" />
					</div>
				</div>
			</section>
		<!-- End Component -->
		<?php if ( $show_home ) : ?>
		<!-- Begin Home -->
			<section class="home wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 medium-10 columns">
						<jdoc:include type="modules" name="home" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Home -->
		<?php endif; ?>
		<?php if ( $show_application ) : ?>
		<!-- Begin Application -->
			<section class="application wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="application" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Application -->
		<?php endif; ?>
		<?php if ( $show_news ) : ?>
		<!-- Begin News -->
			<section class="news wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="news" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End News -->
		<?php endif; ?>
		<?php if ( $show_social_media ) : ?>
		<!-- Begin Social Media -->
			<section class="social_media wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="social_media" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Social Media -->
		<?php endif; ?>
		<?php if ( $show_bottom ) : ?>
		<!-- Begin Bottom -->
			<section class="bottom wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="bottom" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Bottom -->
		<!-- Begin Copyright -->
			<section class="copyright_wrap wow fadeIn" data-wow-delay="0.5s">
				<div class="copyright">
					&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
				</div>
			</section>
		<!-- End Copyright -->
		<?php endif; ?>
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>build/bower_components/jquery/dist/jquery.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/what-input/dist/what-input.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/foundation-sites/dist/js/foundation.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/fancybox/dist/jquery.fancybox.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/wow/dist/wow.min.js"></script>
			<script src="<?php echo $path; ?>build/app.js" type="text/javascript"></script>
		<!-- End Main Scripts -->
	</body>
</html>